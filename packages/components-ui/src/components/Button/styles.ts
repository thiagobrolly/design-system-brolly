import styled from 'styled-components';

export const Button = styled.button`
  width: fit-content;
  padding: 10px;
  font-weight: bold;
  text-align: center;
  border-radius: 5px;
  text-decoration: none;
  cursor: pointer;
`;
